import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auth-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  pagename: String = '';

  constructor() { }

  ngOnInit() {
  }

}
