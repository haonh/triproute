import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth.component';
import { AuthModule as NgxAuthModule } from 'ngx-auth';
import { PROTECTED_FALLBACK_PAGE_URI } from 'ngx-auth';
import { PUBLIC_FALLBACK_PAGE_URI } from 'ngx-auth';
import { AUTH_SERVICE } from 'ngx-auth';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { RecaptchaModule, RecaptchaLoaderService, RECAPTCHA_SETTINGS } from 'ng-recaptcha';
import { AuthLayoutComponent } from './auth-layout/auth-layout.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register/register.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './auth-layout/header/header.component';
import { SidebarComponent } from './auth-layout/sidebar/sidebar.component';
import { FooterComponent } from './auth-layout/footer/footer.component';
import { UserManagementComponent } from './user-management/user-management.component';

const GLOBAL_RECAPTCHA_SETTING = {siteKey: '' };

@NgModule({
  imports: [
    CommonModule,
    NgxAuthModule,
    RecaptchaModule.forRoot(),
    AuthRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    AuthComponent,
    AuthLayoutComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    ResetPasswordComponent,
    ForgotPasswordComponent,
    DashboardComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    UserManagementComponent
  ],
  providers: [
    { provide: RECAPTCHA_SETTINGS, useValue: GLOBAL_RECAPTCHA_SETTING }
    // { provide: RecaptchaLoaderService, useClass:  }
  ],
  exports: []
})
export class AuthModule {
  constructor (private reloadedRecaptcha: RecaptchaLoaderService) {}
}
