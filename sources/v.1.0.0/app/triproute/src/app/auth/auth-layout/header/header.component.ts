import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-auth-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  searchForm: FormGroup;

  constructor(
    private fb: FormBuilder
  ) { 
    this.searchForm = fb.group({
      textSearch: []
    });
  }

  ngOnInit() {
  }

}
