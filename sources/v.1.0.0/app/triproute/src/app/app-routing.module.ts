import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { ProtectedGuard } from 'ngx-auth';

import { AppCustomPreloader } from './app-preloader';

const routes: Routes = [
  {
    path: '',
    loadChildren: 'app/home/home.module#HomeModule',
    data: {
      preload: true
    }
  },
  {
    path: 'admin',
    loadChildren: 'app/auth/auth.module#AuthModule',
    data: {
      preload: true
    }
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash: false, preloadingStrategy: AppCustomPreloader})
  ],
  exports: [RouterModule],
  providers: [AppCustomPreloader]
})
export class AppRoutingModule {}
