import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.scss']
})
export class AuthLayoutComponent implements OnInit {

  pageName: string = '';

  constructor (private router: Router,
  private activatedRoute: ActivatedRoute,
  private renderer: Renderer2) { 
    renderer.addClass(document.body, 'adm-page');
    this.pageName = activatedRoute.snapshot.paramMap.get('pageName');
    console.log(this.pageName);
  }

  ngOnInit () {
    this.addBodyClass(true);
  }

  ngOnDestroy() {
    this.addBodyClass(false);
  }

  private addBodyClass(isAdd): void {
    if (this.pageName != '') {
      if (isAdd) {
        this.renderer.addClass(document.body, this.pageName);
      } else {
        this.renderer.removeClass(document.body, this.pageName);
      }
    }
  }

}
