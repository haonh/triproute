import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthComponent } from './auth.component';
import { PublicGuard } from 'ngx-auth';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LogoutComponent } from './logout/logout.component';
import { LoginComponent } from './login/login.component';
import { AuthLayoutComponent } from './auth-layout/auth-layout.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { UserManagementComponent } from './user-management/user-management.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        component: AuthLayoutComponent,
        // canActivateChild: [ PublicGuard ],
        children: [
          {
            path: 'dashboard',
            component: DashboardComponent,
            data: {
              pageName: 'dashboard'
            }
          },
          {
            path: 'user-management',
            component: UserManagementComponent,
            data:{}
          }
        ]
      },
      {
        path: 'login',
        component: LoginComponent,
        data: {}
      },
      {
        path: 'forgot-password',
        component: ForgotPasswordComponent,
        data: {}
      }
    ],
  },
  {
    path: 'logout',
    component: LogoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
